package INF101.lab1.INF100labs;

import java.util.Scanner;

/**
 * Implement the methods task1, and task2.
 * These programming tasks was part of lab1 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/1/
 */
public class Lab1 {
    
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
    	
        
    }

    public static void task1() {
    	System.out.println("Hei, det er meg, datamaskinen.\nHyggelig å se deg her.\nLykke til med INF101!");
    }

    public static void task2() {
        sc = new Scanner(System.in); // Do not remove this line
        System.out.println("Hva er ditt navn?");
        String navn = sc.nextLine();
        
        System.out.println("Hva er din adresse?");
        String adresse = sc.nextLine();
        
        System.out.println("Hva er ditt postnummer og poststed?");
        String post = sc.nextLine();
        
        System.out.println(navn + "s adresse er:");
        System.out.println();
        System.out.println(navn);
        System.out.println(adresse);
        System.out.println(post);
        
        
        
        
        
        
        
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public static String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.nextLine();
        return userInput;
    }

}
