package INF101.lab1.INF100labs;

import java.lang.reflect.Array;
import java.util.List;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
    	findLongestWords("Game", "Action", "Champion");
    	findLongestWords("apple", "carrot", "ananas");
    	findLongestWords("Four", "Five", "Nine");
    }

    

	public static void findLongestWords(String word1, String word2, String word3) {
    	
		String longest = word1;
		if (word2.length() > longest.length()) {
		    longest = word2;
		}
		if (word3.length() > longest.length()) {
		    longest = word3;
		}
		
		
		System.out.println(longest);
            
    }

    public static boolean isLeapYear(int year) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static boolean isEvenPositiveInt(int num) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

}
